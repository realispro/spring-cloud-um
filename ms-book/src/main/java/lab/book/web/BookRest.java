package lab.book.web;


import lab.book.data.AuthorRepository;
import lab.book.data.BookRepository;
import lab.book.model.Book;
import lab.book.model.Publisher;
import lab.book.service.PublisherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@Slf4j
@RefreshScope
public class BookRest {

    private final PublisherService publisherService;
    private final BookRepository repository;

    private final AuthorRepository authorRepository;

    @Value("${lib.book.log.prefix:'no prefix'}")
    private String validationLogPrefix;

    @GetMapping("/books")
    public List<Book> getBooks() {
        log.info("about to retrieve all books");
        return repository.findAll();
    }

    @GetMapping("/books/{id}")
    public ResponseEntity getBookById(@PathVariable("id") int id) {
        log.info("about to retrieve book {}", id);
        Optional<Book> book = repository.findById(id);
        if (book.isPresent()) {
            return ResponseEntity.ok(book.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/books")
    public ResponseEntity addBook(@RequestBody Book b) {
        log.info("about to add book {}", b);
        Publisher publisher = publisherService.getPublisher(b.getPublisherId());
        if (publisher != null && authorRepository.existsById(b.getAuthor().getId())) {
            return ResponseEntity.status(HttpStatus.CREATED).body(repository.save(b));
        } else {
            log.error("[{}] missing publisher {}", validationLogPrefix, b.getPublisherId());
            return ResponseEntity.badRequest().body("publisher is missing");
        }
    }
}
