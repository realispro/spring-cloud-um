package lab.book.service;

import lab.book.model.Publisher;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "publisher-service", fallback = PublisherServiceFallback.class)
public interface PublisherService {

    @GetMapping("/publishers/{id}")
    //@CircuitBreaker(name="getPublisher", fallbackMethod = "getNullPublisher")
    Publisher getPublisher(@PathVariable("id") int id);

   /* default Publisher getNullPublisher(int id, Throwable t){
        return null;
    }*/
}
