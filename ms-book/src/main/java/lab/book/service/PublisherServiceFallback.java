package lab.book.service;

import lab.book.model.Publisher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PublisherServiceFallback implements PublisherService{
    @Override
    public Publisher getPublisher(int id) {
        return null;
    }
}
