package lab.book.service;

import lab.book.model.Publisher;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

//@Service
@Slf4j
@RequiredArgsConstructor
public class SimplePublisherService implements PublisherService {

    private final CircuitBreakerFactory circuitBreakerFactory;
    private final RestTemplate restTemplate;

    @Override
    //@CircuitBreaker(name="getPublisher", fallbackMethod = "getNullPublisher")
    public Publisher getPublisher(int id) {
        log.info("about to retrieve publisher {}", id);

        CircuitBreaker cb = circuitBreakerFactory.create("getPublisher");

        return cb.run(
                ()-> invokeGetPublisher(id),
                t -> getNullPublisher(id, t)
                );
    }

    private Publisher invokeGetPublisher(int id) {
        String serviceUrl = "http://publisher-service";

        log.info("checking publisher using uri {}", serviceUrl);

        ResponseEntity<Publisher> responseEntity = restTemplate.exchange(
                serviceUrl + "/publishers/" + id,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                Publisher.class);

        return responseEntity.getBody();
    }

    public Publisher getNullPublisher(int id, Throwable t){
        log.error("error while retrieving publisher " + id, t);
        return null;
    }
}
