package lab.publisher.web;


import lab.publisher.data.PublisherRepository;
import lab.publisher.model.Publisher;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class PublisherRest {

    private final PublisherRepository repository;

    @GetMapping("/publishers")
    public List<Publisher> getPublishers() {
        log.info("about to retrieve all publishers");
        return repository.findAll();
    }

    @GetMapping("/publishers/{id}")
    public Publisher getPublisherById(@PathVariable("id") int id) {
        log.info("about to retrieve publisher {}", id);
        return repository.findById(id).orElse(null);
    }

    @PostMapping("/publishers")
    public Publisher addPublisher(@RequestBody Publisher p) {
        log.info("about to add publisher {}", p);
        return repository.save(p);
    }
}
