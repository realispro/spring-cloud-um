package dev.realis.auth.rest;

import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class UserController {

    @GetMapping("/user")
    public Map<String, Object> getUser(OAuth2Authentication auth){
        Map<String, Object> user = new HashMap<>();

        user.put("username", auth.getUserAuthentication().getPrincipal());
        user.put("authorities", auth.getUserAuthentication().getAuthorities());

        return user;
    }
}
