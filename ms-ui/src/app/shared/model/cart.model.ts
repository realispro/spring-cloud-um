export interface Cart {
    id?: number;
    items?: Array<Item>;
    totalPrice?: number;
}

export interface Item {
  productId?: number;
  productName?: string;
  price?: number;
}

