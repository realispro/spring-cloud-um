// Generated using typescript-generator version 2.0.400 on 2020-08-05 19:45:35.

export interface Publisher {
    id?: number;
    name?: string;
    logo?: string;
}

export const enum EmployeeActionEnum {
    CREATE = 'CREATE',
    UPDATE = 'UPDATE',
    DELETE = 'DELETE',
}
