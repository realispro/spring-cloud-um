export class AppSettings {

    public static APP_NAME = 'Librarian';

    public static SERVER_URL = 'http://localhost:5555';

    public static API_ENDPOINT = '';

    public static LOGIN_ENDPOINT = AppSettings.SERVER_URL + AppSettings.API_ENDPOINT + '/auth-service/oauth/token';

    public static USER_ENDPOINT = AppSettings.SERVER_URL + AppSettings.API_ENDPOINT + '/auth-service/user';

    public static ACCESS_TOKEN = 'access_token';

    public static CURRENT_USER = 'current_user';

    public static PUBLISHER_ENDPOINT = AppSettings.SERVER_URL + AppSettings.API_ENDPOINT + "/publisher-service/publishers";

    public static BOOK_ENDPOINT = AppSettings.SERVER_URL + AppSettings.API_ENDPOINT + "/book-service/books";

    public static CART_ENDPOINT = AppSettings.SERVER_URL + AppSettings.API_ENDPOINT + "/shopping-cart-service/carts";

    public static AUTHORIZATION_HEADER_NAME = 'Authorization';
}
