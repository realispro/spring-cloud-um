import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {BookService} from '../../../shared/service/book.service';
import {PublisherService} from '../../../shared/service/publisher.service';
import {Location} from '@angular/common';
import {takeUntil} from 'rxjs/operators';
import {Publisher} from "../../../shared/model/publisher.model";

@Component({
  selector: 'app-employee-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {

  public editBookForm: FormGroup;
  submitted = false;
  private _$alive = new Subject();
  public publishers: Publisher[];

  constructor(private _formBuilder: FormBuilder,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _bookService: BookService,
    private _publisherService: PublisherService,
    private _location: Location) { }

  ngOnInit(): void {
    this.createForm();
    this._publisherService.fetchAll().subscribe(result => {
      this.publishers = <Array<Publisher>>result;
    });
    this._activatedRoute.params.pipe(takeUntil(this._$alive)).subscribe(params => {
      let id = params['id'];
      this._bookService.fetchById(id).subscribe(result => {
        this.editBookForm.get('id').setValue(result['id']);
        this.editBookForm.get('title').setValue(result['title']);
        this.editBookForm.get('author').setValue(result['author']);
        this.editBookForm.get('cover').setValue(result['cover']);
        this.editBookForm.get('price').setValue(result['price']);
        this.editBookForm.get('publisherId').setValue(result['publisherId']);

      });
    });
  }

  private createForm() {
    this.editBookForm = this._formBuilder.group({
      id: ['', Validators.required],
      title: ['', Validators.required],
      author: ['', Validators.required],
      price: ['', Validators.required],
      cover: ['', Validators.required],
      publisherId: ['', Validators.required]

    });
  }

  get inputFieldValue() {
    return this.editBookForm.controls;
  }

  public submit() {
    this.submitted = true;
    if (this.editBookForm.invalid) {
      return;
    }
    this._bookService.update(this.editBookForm.value).subscribe(result => {
      this._router.navigate(['book']);
    });
  }

  cancel() {
    this.editBookForm.get('title').reset();
    this.editBookForm.get('author').reset();
    this.editBookForm.get('cover').reset();
    this.editBookForm.get('price').reset();
    this.editBookForm.get('publisherId').reset();
  }

  back() {
    this._location.back();
  }

}
