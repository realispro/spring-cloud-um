import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PublisherService} from '../../../shared/service/publisher.service';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-department-add',
  templateUrl: './publisher-add.component.html',
  styleUrls: ['./publisher-add.component.css']
})
export class PublisherAddComponent implements OnInit {

  public addPublisherForm: FormGroup;
  submitted = false;

  constructor(private _formBuilder: FormBuilder,
    private _router: Router,
    private _departmentService: PublisherService,
    private _location: Location) {
  }

  ngOnInit(): void {
    this.createForm();
  }

  private createForm() {
    this.addPublisherForm = this._formBuilder.group({
      name: ['', Validators.required],
      logo: ['', Validators.required]
    });
  }

  get inputFieldValue() {
    return this.addPublisherForm.controls;
  }

  public submit() {
    this.submitted = true;
    if (this.addPublisherForm.invalid) {
      return;
    }
    this._departmentService.save(this.addPublisherForm.value).subscribe(result => {
      this._router.navigate(['department']);
    });
  }

  cancel() {
    this.addPublisherForm.reset();
  }

  back() {
    this._location.back();
  }


}
