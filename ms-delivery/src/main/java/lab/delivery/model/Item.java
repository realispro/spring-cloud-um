package lab.delivery.model;

import lombok.Data;

@Data
public class Item /*implements Serializable */{

    private int productId;
    private String productName;
    private float price;
    private int amount;
}
