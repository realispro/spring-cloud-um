package lab.delivery;

import lab.delivery.model.Cart;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Consumer;

@Slf4j
@Configuration
public class OrderConsumer {

    /*@StreamListener(Sink.INPUT)
    public void consume(Cart cartString){
        log.info("order received: {}", cartString);
    }*/

    @Bean
    public Consumer<Cart> consumer(){
        return cart -> log.info("cart received: {}", cart);
    }
}
